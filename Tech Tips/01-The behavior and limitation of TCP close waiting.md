* Topology


```mermaid
graph TD
A["NPort<br>(Reverse RealCOM)"] --> |TCP connect| B["NPort Driver Manager<br>(PC)"];
```

* Issue

    NPort Server (192.168.127.254:8010)

    NPort Driver Manager (192.168.127.111:60967)

    TCP 連線斷線後過了大約 30 秒才能恢復連線

    ![alt text](01-disconnect-packet.png)


* Discussing

    1. 查看目前連線狀態 netstat -bn

        ![alt text](01-netstat.png)


    2. What is TIME_WAIT?

        ![alt text](01-time-wait.png)

        ([參考文件](https://dev.twsiyuan.com/2017/09/tcp-states.html))

    
    3. How to adjust TIME_WAIT?

        ![alt text](01-ms-time-wait.png)
    
        ([參考文件](https://learn.microsoft.com/zh-tw/biztalk/technical-guides/settings-that-can-be-modified-to-improve-network-performance))

    4. How to reestablish connection?
    
        * [使用 SO_REUSEADDR SO_EXCLUSIVEADDRUSE 及注意事項](https://learn.microsoft.com/zh-tw/windows/win32/winsock/using-so-reuseaddr-and-so-exclusiveaddruse)

        * [注意不同 OS 間 SO_REUSEADDR 及 SO_REUSEPORT 的差異](https://stackoverflow.com/questions/14388706/how-do-so-reuseaddr-and-so-reuseport-differ)

        * 不指定 TCP client 的 local port (改為0)
        
            ![alt text](01-nport-ports.png)




